# Cryogenics-SMS Device Server

This project allows to control Cryogenics SMS power supplies via LAN.

Control of [Cryogenics SMS-120](https://www.cryogenic.co.uk/products/superconducting-magnet-power-supplies) superconducting magnet power supply is possible via USB only.
It does not look to be the original design, because apparently, an FTDI chip has been incorporated into the power supply later.
The presumably earlier serial interface would actually have been better for us.

## Idea

A raspberry pi (or any other local pc) acts as USB to ethernet server and provides access to the USB-connected FTDI-translated serial interfaces of the power supplies.
The four powersupplies are identified via FTDI Vendor ID, Product ID and serial number.
Udev creates individual `/dev` entries upon connection with USB.
[Socat](http://www.dest-unreach.org/socat/) relays the data traffic between USB and Ethernet (TCP).


## Power Supplies

The individual power supplies can be identified via their serial numbers:
```
PSU 01: idVendor: 0403; idProduct: 6001; serial: FTB3M4G9
PSU 02: idVendor: 0403; idProduct: 6001; serial: FTGHUEJL
PSU 03: idVendor: 0403; idProduct: 6001; serial: FTGHUC89
PSU 04: idVendor: 0403; idProduct: 6001; serial: FTGHUEIB
```

## Raspberry Pi

The RPi has a default raspian OS.
Any other Linux on any PC will do as well.
At the time of writing this host was used:
```
Hostname: cwrpi05.gsi.de
IP: 10.10.43.35
```

## udev

The udev rules follow this template (see file cryogenics.rules for all rules):
```
ACTION=="add", SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="FTB3M4G9", GROUP:="plugdev", MODE:="0660", SYMLINK+="cryotty01
```

## socat

socat pairs the udev-created device with a TCP port.
Each device/port is configured as follows:
```
socat -d -d TCP-LISTEN:4001,fork /dev/cryotty01,raw
```

Automatic configuration is managed with systemd (see files `/etc/systemd/system/socat_cryo0[1-4].service`).

## Usage

The TCP ports can be tested via telnet:

```
telnet cwrpi05 4001
```

Productive use is realized with EPICS on another PC.

## Known Problems

- Sometimes the power supplies send the character DC2, but one does not know when and why.... (this will cause mismatches and corresponding log entries in EPICS StreamDevice Support - not a big problem really, but annoying)
- The devices do not react when everything has just been powered-up. First, communication from the local PC/RPi via USB must be done - afterwards the TCP port will work as well .... (use initialize_psu_communication.py util to do so)
