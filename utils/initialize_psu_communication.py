#!/usr/bin/python3

# This script has been written to automate a first communication exchange
# between the cryogenics PSU and the USB host (PC/Raspberry) connected to the PSUs.
# This seems to be required once, after powering up both the PSU(s) and the PC, for some reason.
# Use it locally on the USB host.

import serial
import time

ser1 = serial.Serial('/dev/cryotty01', timeout=3)
ser2 = serial.Serial('/dev/cryotty02', timeout=3)
ser3 = serial.Serial('/dev/cryotty03', timeout=3)
ser4 = serial.Serial('/dev/cryotty04', timeout=3)

interfaces = [ser1, ser2, ser3, ser4]

print("Probing following interfaces:")
for ifc in interfaces:
    print(ifc.name)

print("Getting output readback:")
for ifc in interfaces:
    print(f"Asking {ifc.name} ...")
    ifc.write(b'GET OUTPUT\n')
time.sleep(0.1)
print("Replys:")
for ifc in interfaces:
    print(f"{ifc.name}: {ifc.readline()}")
time.sleep(0.3)
for ifc in interfaces:
    ifc.close()

